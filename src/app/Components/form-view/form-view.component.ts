import { Component, OnInit, HostListener, ViewChild } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { NgForm } from '@angular/forms';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  stagger,
  query
} from '@angular/animations';

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
  animations: [
    trigger(
      'myAnimation',
      [
        transition('* => *', [
          query(':leave', [
            stagger('10000ms', [
              style({ transform: 'translateX(100%)', 'opacity': 1 }),
              animate('100ms', style({ transform: 'translateX(-100%)', 'opacity': 0 }))
            ])
          ], { optional: true }
          ),
          query(':enter', [
            style({ transform: 'translateX(100%)', opacity: 0 }),
            animate('500ms', style({ transform: 'translateX(0)', 'opacity': 1 }))

          ], { optional: true }
          )
        ])]
    ),
    trigger('fadeInOut', [
      transition('void => *', [
        style({ opacity: 0 }), // Style only for transition transition (after transiton it removes)
        animate(400, style({ opacity: 1 })) // the new state of the transition(after transiton it removes)
      ]),
      transition('* => void', [
        animate(400, style({ opacity: 0 })) // the new state of the transition(after transiton it removes)
      ])
    ])
  ]
})
export class FormViewComponent implements OnInit {

  @ViewChild('ciudades') ciudades: any;
  @ViewChild('nombres') nombres: any;
  @ViewChild('apellidos') apellidos: any;
  @ViewChild('email') email: any;

  public formType: string;
  public idCruise: number;
  public largeFormSteps: number;
  public shortFormSteps: number;
  public wizardAnimation: boolean;
  public bindingVar = '';
  public isDropdownVisible = false;
  public startDateVisible = false;
  public endDateVisible = false;
  public adults: number;
  public kids: number;
  public regionName = '';
  public cabinType = '';
  public consultant = '';
  public consultantName = '';
  public name: string;
  public textValue: string;

  public validationErrors = false;

  public unamePattern = '^[a-z0-9_-]{8,15}$';
  public mobnumPattern = '^((\\+91-?)|0)?[0-9]{10}$';
  public emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  validateEmail = true;

  private dates = {
    departure: '',
    returning: ''
  };
  private calendarLimits: {
    start: {
      min: '',
      max: ''
    },
    end: {
      min: '',
      max: ''
    }
  };

  constructor(
    private route: ActivatedRoute
  ) {
    this.formType = 'large';
    this.idCruise = +this.route.snapshot.paramMap.get('idCruise') || null;
    this.largeFormSteps = 1;
    this.shortFormSteps = 1;
    this.adults = 1;
    this.kids = 0;
  }

  onNext() {
    if (this.formType === 'large') {
      if (this.checkValidation(this.largeFormSteps)) {
        this.largeFormSteps++;
      }
    }
    if (this.formType === 'short') {
      if (this.checkValidationShort(this.shortFormSteps)) {
        this.shortFormSteps++;
      }
    }
  }

  checkValidation(currentStepLarge) {
    this.validationErrors = false;
    switch (currentStepLarge) {
      case 1:
        if (this.ciudades.nativeElement.value !== '') {
          return true;
        }
        this.validationErrors = true;
        if (this.regionName !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      case 2:
      case 3:
        return true;
      case 4:
        if (this.cabinType !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      case 5:
        if (this.nombres.nativeElement.value !== '') {
          return true;
        }
        if (this.apellidos.nativeElement.value !== '') {
          return true;
        }
        if (this.email.nativeElement.value !== '') {
          return true;
        }

        if (this.consultant !== 'No' && this.consultantName !== '') {
          return true;
        }

        this.validationErrors = true;
        return false;
      default:
        return false;
    }
  }

  checkValidationShort(currentStepShort) {
    this.validationErrors = false;
    switch (currentStepShort) {
      case 1:
        return true;
      case 2:
        if (this.cabinType !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      case 3:
        if (this.nombres.nativeElement.value !== '') {
          return true;
        }
        if (this.apellidos.nativeElement.value !== '') {
          return true;
        }
        if (this.email.nativeElement.value !== '') {
          return true;
        }
        this.validationErrors = true;
        return false;
      default:
        return false;
    }
  }

  onBack() {
    if (this.formType === 'large') {
      if (this.largeFormSteps > 1) {
        this.largeFormSteps = this.largeFormSteps - 1;
        this.wizardAnimation = false;
      }
    }
    if (this.formType === 'short') {
      if (this.shortFormSteps > 1) {
        this.shortFormSteps = this.shortFormSteps - 1;
        this.wizardAnimation = false;
      }
    }
  }

  onClickStartDateVisible() {
    if (this.endDateVisible === true) {
      this.endDateVisible = !this.endDateVisible;
      this.startDateVisible = !this.startDateVisible;
    } else {
      this.startDateVisible = !this.startDateVisible;
    }
  }

  onClickEndDateVisible() {
    if (this.startDateVisible === true) {
      this.startDateVisible = !this.startDateVisible;
      this.endDateVisible = !this.endDateVisible;
    } else {
      this.endDateVisible = !this.endDateVisible;
    }
  }

  onClickIsDropdownVisible() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }

  onAddAdults() {
    this.adults = this.adults + 1;
  }

  onRemoveAdults() {
    if (this.adults > 1) {
      this.adults = this.adults - 1;
    }
  }

  onAddKids() {
    this.kids = this.kids + 1;
  }

  onRemoveKids() {
    if (this.kids > 0) {
      this.kids = this.kids - 1;
    }
  }

  onClickSetRegionName() {
    this.regionName = this.regionName;
  }

  onClickCabinType() {
    this.cabinType = this.cabinType;
  }

  onClickHasConsultant() {
    this.consultant = this.consultant;
  }


  ngOnInit() {
  }

  getFormBackgroundDesktop() {
    if (this.formType === 'large') {
      return 'assets/img/Desktop/formulario-desktop.jpg';
    }
    if (this.formType === 'short') {
      return 'assets/img/Desktop/islas-griegas-desde-venecia-desktop.jpg';
    }
  }

  getFormBackgroundMobile() {
    if (this.formType === 'large') {
      return 'assets/img/Mobile/formulario-mobile.jpg';
    }
    if (this.formType === 'short') {
      return 'assets/img/Mobile/islas-griegas-desde-venecia-mobile.jpg';
    }
  }

  // ANIMATION CONFIG BEGINS

  fadeIn() {
    this.bindingVar = 'fadeIn';
  }
  fadeOut() {
    this.bindingVar = 'fadeOut';
  }
  toggle() {
    this.bindingVar === 'fadeOut' ? this.fadeIn() : this.fadeOut();
  }

}
