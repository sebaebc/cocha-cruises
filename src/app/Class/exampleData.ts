import { ProductInfo } from "./Product-info";

export const DetalleProducto: ProductInfo = {
    id: 1,
    image:{
        alt: "Texto de prueba",
        url: "assets/img/Desktop/islas-griegas-desde-venecia-desktop.jpg"
    },
    title: "Islas Griegas desde Venecia",
    cruise: "Norwegian Star",
    company: "Norwegian Cruise Line",
    days: 10,
    price: {
      main:{
        currency: "USD",
        price: 1980
      },
      second:{
        currency: "CLP",
        price: 176487
      }  
    },
    discount: 20,
    description: "Venecia, excelente para un primer crucero, la ocasión de visitar el Palacio del Doges, se puede admirar el ballet de las góndolas que deslizan sobre el Gran Canal. Para los enamorados un crucero desde Venecia es ideal. Pasearse en los más prestigiosos jardines como Papadopoli y el jardin Groggia cuentan entre los más bonitos paisajes del Séréniossime. Excelente para un primer crucero, la ocasión de visitar el Palacio de Doges, se puede admirar el ballet de las góndolas que deslizan sobre el gran canal. Para los enamorados un crucero desde Venecia es ideal. Pasearse en los más prestigiosos jardines como Papadopoli y el jardin Groggia cuentan entre los más bonitos paisajes del Séréniossime.",
    destinations: [
        {
            city: "Venecia",
            country: "Italia"
        },
        {
            city: "Kotor",
            country: "Montenegro"
        },
        {
            city: "Corfú",
            country: "Grecia"
        },
        {
            city: "Santorini",
            country: "Grecia"
        },
        {
            city: "Mykonos",
            country: "Grecia"
        },
        {
            city: "Dubrovnik",
            country: "Croacia"
        }
    ],
    itinerary: [
        {
            day: 1,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-1-venecia.jpg"
            },
            destination:{
                city: "Venecia",
                country: "Italia"
            },
            departure: "17:00"
        },
        {
            day: 2,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-2-kotor.jpg"
            },
            destination:{
                city: "Kotor",
                country: "Grecia"
            },
            arrival: "14:00",
            departure: "16:30",
            optional: "Incluye excursión"
        },
        {
            day: 3,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-3-corfu.jpg"
            },
            destination:{
                city: "Corfú",
                country: "Grecia"
            },
            arrival: "09:00",
            departure: "20:00"
        },
        {
            day: 4,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-4-dia-de-navegacion.jpg"
            }
        },
        {
            day: 5,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-5-mykonos.jpg"
            },
            destination:{
                city: "Mykonos",
                country: "Grecia"
            },
            arrival: "06:30",
            departure: "13:00"
        },
        {
            day: 6,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 6,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 7,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 8,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 9,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 10,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 11,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 12,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        },
        {
            day: 13,
            image: {
                alt: "Texto de prueba",
                url: "assets/img/Desktop/itinerario desktop/itinerario-dia-6-dubrovnik.jpg"
            },
            destination:{
                city: "Dubrovnik",
                country: "Croacia"
            },
            arrival: "09:00"
        }
    ],
    images: [
        {
            url: "assets/img/Desktop/a-proposito-del-barco-desktop.jpg",
            alt: ""
        },{
            url: "assets/img/Desktop/crucero.jpg",
            alt: ""
        }
    ],
    amenities: [
        {
            icon: "cch-culture",
            text: "Animaciones Culturales"
        },
        {
            icon: "cch-casino",
            text: "Casino"
        },
        {
            icon: "cch-spa",
            text: "Spa"
        },
        {
            icon: "cch-gym",
            text: "Gimnasio"
        },
        {
            icon: "cch-baby",
            text: "Guarderia"
        },
        {
            icon: "cch-amenities-entertainment",
            text: "Entretenimiento"
        },
        {
            icon: "cch-wifi",
            text: "Wi-Fi"
        },
        {
            icon: "cch-pool",
            text: "Piscina"
        },
        {
            icon: "cch-library",
            text: "Biblioteca"
        },
        {
            icon: "cch-coffee-shop",
            text: "Cafeteria"
        },
        {
            icon: "cch-massage",
            text: "Masaje"
        },
        {
            icon: "cch-hidromassage",
            text: "Hidromasaje"
        },
        {
            icon: "cch-dinner",
            text: "Cena"
        },
        {
            icon: "cch-computers-room",
            text: "Sala de Computadores"
        },
        {
            icon: "cch-nautical-sport",
            text: "Actividades Acuáticas"
        }
    ],
    cabin: [
        {
            id: 1,
            title: "Cabinas internas",
            text: "",
            images: [
                {
                    url: "assets/img/Desktop/cabinas-internas-desktop.jpg"
                }
            ],
            price: {
                main:{
                    currency: "USD",
                    price: 2000
                },
                second:{
                    currency: "CLP",
                    price: 1186181
                } 
            },
            discount: 20,
            datePrice: [
                {
                    date: "14/03/2018",
                    price: 2000,
                    category: "low"
                },
                {
                    date: "09/03/2018",
                    price: 2200,
                    category: "mid"
                },
                {
                    date: "27/03/2018",
                    price: 2230,
                    category: "mid"
                },
                {
                    date: "30/03/2018",
                    price: 3400,
                    category: "high"
                }
            ]
        }
    ],
    similar: [
        {
            id: 23,
            title: "Europa mediterranea",
            days: 10,
            subtitle: "por Europa Mediterranea y Medio Oriente",
            image: {
                url: "assets/img/Desktop/islas-griegas-desde-venecia-desktop.jpg"
            },
            price: {
                main:{
                    currency: "USD",
                    price: 2100
                },
                second:{
                    currency: "CLP",
                    price: 1245626
                } 
            },
            discount: 20,
            cities: [
                {
                    city: "Venecia",
                    country: "Italia"
                },
                {
                    city: "Kotor",
                    country: "Montenegro"
                },
                {
                    city: "Corfú",
                    country: "Grecia"
                },
                {
                    city: "Santorini",
                    country: "Grecia"
                },
                {
                    city: "Mykonos",
                    country: "Grecia"
                },
                {
                    city: "Dubrovnik",
                    country: "Croacia"
                }
            ]
        },
        {
            id: 72,
            title: "Mar Caribe",
            days: 12,
            subtitle: "por Caribe, cuba, Bahamas",
            image: {
                url: "assets/img/Desktop/playa.jpg"
            },
            price: {
                main:{
                    currency: "USD",
                    price: 1500
                },
                second:{
                    currency: "CLP",
                    price: 889733
                } 
            },
            discount: 20,
            cities: [
                {
                    city: "Habana",
                    country: "Cuba"
                },
                {
                    city: "Nasáu",
                    country: "Bahamas"
                },
                {
                    city: "Punta Cana",
                    country: "República Dominicana"
                }
            ]
        }
    ]
}