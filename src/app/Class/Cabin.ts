import { Image } from "./Image";
import { Price } from "./Price";
import { DatePrice } from "./Date-price";

export class Cabin {
    id: number;
    title: string;
    text: string;
    images: Image[];
    price: Price;
    discount: number;
    datePrice: DatePrice[];
}
